﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageManualClassification
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();

            this.Text = appName + appVersion;
            this.Size = new Size(1500, 1000);
            AlignFormControlsWhileResizing();
            
        }

        // public VARS:
        string appName = "Image Manual Classification";
        string appVersion = " v.0.0.4";
        
        ImagesForClassification IFC = new ImagesForClassification();

        private void LoadAndDisplayImage()
        {
            // Load Set:
            IFC.LoadImagesToTheList(tb_classificationFolder.Text);
            IFC.SortListOfImages();

            // Display First Image:
            IFC.DisplayImage(pictureBox1);
            IFC.GetClass();
            ColorButtonsBasedOnType();
        }

        private void SaveFormSettings()
        {
            Properties.Settings.Default.dirClassificationFolder = tb_classificationFolder.Text;
            Properties.Settings.Default.dirTypeA = tb_typeA.Text;
            Properties.Settings.Default.dirTypeB = tb_typeB.Text;
            Properties.Settings.Default.dirTypeC = tb_typeC.Text;
            Properties.Settings.Default.dirTypeUndefined = tb_undefined.Text;
            Properties.Settings.Default.dirTypeDelete = tb_delete.Text;
            Properties.Settings.Default.formSize = this.Size;
            Properties.Settings.Default.nameTypeA = tb_typeA_name.Text;
            Properties.Settings.Default.nameTypeB = tb_typeB_name.Text;
            Properties.Settings.Default.nameTypeC = tb_typeC_name.Text;
            Properties.Settings.Default.nameTypeUnedfined = tb_undefined_name.Text;
            Properties.Settings.Default.nameTypeDelete = tb_delete_name.Text;
            Properties.Settings.Default.Save();
        }

        private void LoadFormSettings()
        {
            tb_classificationFolder.Text = Properties.Settings.Default.dirClassificationFolder;
            tb_typeA.Text = Properties.Settings.Default.dirTypeA;
            tb_typeB.Text = Properties.Settings.Default.dirTypeB;
            tb_typeC.Text = Properties.Settings.Default.dirTypeC;
            tb_undefined.Text = Properties.Settings.Default.dirTypeUndefined;
            tb_delete.Text = Properties.Settings.Default.dirTypeDelete;
            tb_typeA_name.Text = Properties.Settings.Default.nameTypeA;
            tb_typeB_name.Text = Properties.Settings.Default.nameTypeB;
            tb_typeC_name.Text = Properties.Settings.Default.nameTypeC;
            tb_undefined_name.Text = Properties.Settings.Default.nameTypeUnedfined;
            tb_delete_name.Text = Properties.Settings.Default.nameTypeDelete;
            this.Size = Properties.Settings.Default.formSize;
        }

        private void SetButtonsTextByType()
        {
            btn_typeA.Text = tb_typeA_name.Text;
            btn_typeB.Text = tb_typeB_name.Text;
            btn_typeC.Text = tb_typeC_name.Text;
            btn_Undefined.Text = tb_undefined_name.Text;
            btn_delete.Text = tb_delete_name.Text;
        }

        private void AlignFormControlsWhileResizing()
        {
            tabControl1.Width = this.Width - 15;
            tabControl1.Height = this.Height - 60;

            pictureBox1.Width = tabControl1.Width - 10;
            pictureBox1.Height = tabControl1.Height - 180;

            gb_commands.Location = new Point(10, pictureBox1.Height + 10);
        }

        private void ColorButtonsBasedOnType()
        {
            ColorButtonsBasedOnTypeDisableAll();

            if (IFC.GetClass() == TheImage.ClassType.TypeA) { btn_typeA.BackColor = IFC.GetCurrentimage().GetColorByClass(); }
            if (IFC.GetClass() == TheImage.ClassType.TypeB) { btn_typeB.BackColor = IFC.GetCurrentimage().GetColorByClass(); }
            if (IFC.GetClass() == TheImage.ClassType.TypeC) { btn_typeC.BackColor = IFC.GetCurrentimage().GetColorByClass(); }
            if (IFC.GetClass() == TheImage.ClassType.UNDEFINED) { btn_Undefined.BackColor = IFC.GetCurrentimage().GetColorByClass(); }
            if (IFC.GetClass() == TheImage.ClassType.DELETE) { btn_delete.BackColor = IFC.GetCurrentimage().GetColorByClass(); }
        }

        public void ColorButtonsBasedOnTypeDisableAll()
        {
            btn_typeA.BackColor = Color.Transparent;
            btn_typeB.BackColor = Color.Transparent;
            btn_typeC.BackColor = Color.Transparent;
            btn_Undefined.BackColor = Color.Transparent;
            btn_delete.BackColor = Color.Transparent;
        }

        private void Set_NextImage()
        {
            IFC.SaveImageClassType();
            IFC.NextImage();
            IFC.DisplayImage(pictureBox1);
            IFC.GetCurrentimage().GetClassByDestinationFolder();
            ColorButtonsBasedOnType();
            UpdateGUItext();
        }

        private void Set_PreviousImage()
        {
            IFC.SaveImageClassType();
            IFC.PreviousImage();
            IFC.DisplayImage(pictureBox1);
            IFC.GetCurrentimage().GetClassByDestinationFolder();
            ColorButtonsBasedOnType();
            UpdateGUItext();
        }

        private void UpdateGUItext()
        {
            tb_currentImageIndex.Text = (IFC.currentImageIndex + 0).ToString(); ;
            lbl_numImages.Text = "/ " + (IFC.listOfImages.Count - 1).ToString();
            lbl_imageName.Text = IFC.GetImageFolder();
        }

        private void KeyboardShortcuts(KeyEventArgs key)
        {
            if (key.KeyData == Keys.A)
            {
                IFC.SetClassification(TheImage.ClassType.TypeA);
                ColorButtonsBasedOnType();
            }
            if (key.KeyData == Keys.S)
            {
                IFC.SetClassification(TheImage.ClassType.TypeB);
                ColorButtonsBasedOnType();
            }
            if (key.KeyData == Keys.D)
            {
                IFC.SetClassification(TheImage.ClassType.TypeC);
                ColorButtonsBasedOnType();
            }
            if (key.KeyData == Keys.F)
            {
                IFC.SetClassification(TheImage.ClassType.UNDEFINED);
                ColorButtonsBasedOnType();
            }
            if (key.KeyData == Keys.J)
            {
                IFC.SetClassification(TheImage.ClassType.DELETE);
                ColorButtonsBasedOnType();
            }
            if (key.KeyData == Keys.K)
            {
                Set_PreviousImage();
            }
            if (key.KeyData == Keys.L)
            {
                Set_NextImage();
            }
            if (key.KeyData == Keys.N)
            {
                IFC.SetClassification(TheImage.ClassType.DELETE);
                ColorButtonsBasedOnType();
                Set_PreviousImage();
            }
        }

        // BUTTONS:

        private void btn_Load_Click(object sender, EventArgs e)
        {
            LoadAndDisplayImage();
            IFC.GetCurrentimage().GetClassByDestinationFolder();
            UpdateGUItext();
        }

        private void btn_typeA_Click(object sender, EventArgs e)
        {
            IFC.SetClassification(TheImage.ClassType.TypeA);
            ColorButtonsBasedOnType();
        }

        private void btn_typeB_Click(object sender, EventArgs e)
        {
            IFC.SetClassification(TheImage.ClassType.TypeB);
            ColorButtonsBasedOnType();
        }

        private void btn_typeC_Click(object sender, EventArgs e)
        {
            IFC.SetClassification(TheImage.ClassType.TypeC);
            ColorButtonsBasedOnType();
        }

        private void btn_Undefined_Click(object sender, EventArgs e)
        {
            IFC.SetClassification(TheImage.ClassType.UNDEFINED);
            ColorButtonsBasedOnType();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            IFC.SetClassification(TheImage.ClassType.DELETE);
            ColorButtonsBasedOnType();
        }

        private void btn_NextImage_Click(object sender, EventArgs e)
        {
            Set_NextImage();
        }

        private void btn_PreviousImage_Click(object sender, EventArgs e)
        {
            Set_PreviousImage();
        }

        private void btn_Previous_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveFormSettings();
        }

        private void btn_Previous_Load(object sender, EventArgs e)
        {
            LoadFormSettings();
            SetButtonsTextByType();
        }

        private void btn_Previous_Resize(object sender, EventArgs e)
        {
            AlignFormControlsWhileResizing();
        }

        private void btn_goToIndexNumber_Click(object sender, EventArgs e)
        {
            IFC.GoToImageIndex(int.Parse(tb_currentImageIndex.Text));
            IFC.DisplayImage(pictureBox1);
            IFC.GetCurrentimage().GetClassByDestinationFolder();
            ColorButtonsBasedOnType();
            UpdateGUItext();
        }

        private void tb_ShortcutsKeyCollector_KeyDown(object sender, KeyEventArgs key)
        {
            KeyboardShortcuts(key);
        }

        private void tb_ShortcutsKeyCollector_Click(object sender, EventArgs e)
        {
            tb_ShortcutsKeyCollector.Clear();
        }
    }
}
