﻿namespace ImageManualClassification
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gb_commands = new System.Windows.Forms.GroupBox();
            this.lbl_imageName = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_ShortcutsKeyCollector = new System.Windows.Forms.TextBox();
            this.btn_goToIndexNumber = new System.Windows.Forms.Button();
            this.lbl_numImages = new System.Windows.Forms.Label();
            this.tb_currentImageIndex = new System.Windows.Forms.TextBox();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_Load = new System.Windows.Forms.Button();
            this.btn_Undefined = new System.Windows.Forms.Button();
            this.btn_typeA = new System.Windows.Forms.Button();
            this.btn_PreviousImage = new System.Windows.Forms.Button();
            this.btn_typeB = new System.Windows.Forms.Button();
            this.btn_NextImage = new System.Windows.Forms.Button();
            this.btn_typeC = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_delete_name = new System.Windows.Forms.TextBox();
            this.tb_delete = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_undefined_name = new System.Windows.Forms.TextBox();
            this.tb_typeC_name = new System.Windows.Forms.TextBox();
            this.tb_classificationFolder_name = new System.Windows.Forms.TextBox();
            this.tb_typeB_name = new System.Windows.Forms.TextBox();
            this.tb_typeA_name = new System.Windows.Forms.TextBox();
            this.tb_undefined = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_typeC = new System.Windows.Forms.TextBox();
            this.tb_classificationFolder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_typeB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_typeA = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lbl_About = new System.Windows.Forms.Label();
            this.lbl_key_typeA = new System.Windows.Forms.Label();
            this.lbl_key_typeB = new System.Windows.Forms.Label();
            this.lbl_key_typeC = new System.Windows.Forms.Label();
            this.lbl_key_typeUndefined = new System.Windows.Forms.Label();
            this.lbl_key_typeDelete = new System.Windows.Forms.Label();
            this.lbl_key_previous = new System.Windows.Forms.Label();
            this.lbl_key_next = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gb_commands.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gray;
            this.pictureBox1.Location = new System.Drawing.Point(6, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(993, 719);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1530, 1053);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gb_commands);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Location = new System.Drawing.Point(8, 39);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1514, 1006);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Classification";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gb_commands
            // 
            this.gb_commands.Controls.Add(this.label10);
            this.gb_commands.Controls.Add(this.lbl_key_next);
            this.gb_commands.Controls.Add(this.lbl_key_previous);
            this.gb_commands.Controls.Add(this.lbl_key_typeDelete);
            this.gb_commands.Controls.Add(this.lbl_key_typeUndefined);
            this.gb_commands.Controls.Add(this.lbl_key_typeC);
            this.gb_commands.Controls.Add(this.lbl_key_typeB);
            this.gb_commands.Controls.Add(this.lbl_key_typeA);
            this.gb_commands.Controls.Add(this.lbl_imageName);
            this.gb_commands.Controls.Add(this.label9);
            this.gb_commands.Controls.Add(this.tb_ShortcutsKeyCollector);
            this.gb_commands.Controls.Add(this.btn_goToIndexNumber);
            this.gb_commands.Controls.Add(this.lbl_numImages);
            this.gb_commands.Controls.Add(this.tb_currentImageIndex);
            this.gb_commands.Controls.Add(this.btn_delete);
            this.gb_commands.Controls.Add(this.btn_Load);
            this.gb_commands.Controls.Add(this.btn_Undefined);
            this.gb_commands.Controls.Add(this.btn_typeA);
            this.gb_commands.Controls.Add(this.btn_PreviousImage);
            this.gb_commands.Controls.Add(this.btn_typeB);
            this.gb_commands.Controls.Add(this.btn_NextImage);
            this.gb_commands.Controls.Add(this.btn_typeC);
            this.gb_commands.Location = new System.Drawing.Point(6, 731);
            this.gb_commands.Name = "gb_commands";
            this.gb_commands.Size = new System.Drawing.Size(1502, 264);
            this.gb_commands.TabIndex = 8;
            this.gb_commands.TabStop = false;
            this.gb_commands.Text = "Commands";
            // 
            // lbl_imageName
            // 
            this.lbl_imageName.AutoSize = true;
            this.lbl_imageName.Location = new System.Drawing.Point(202, 233);
            this.lbl_imageName.Name = "lbl_imageName";
            this.lbl_imageName.Size = new System.Drawing.Size(126, 25);
            this.lbl_imageName.TabIndex = 12;
            this.lbl_imageName.Text = "imageName";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(298, 25);
            this.label9.TabIndex = 11;
            this.label9.Text = "Keyboard Shortcuts Collector:";
            // 
            // tb_ShortcutsKeyCollector
            // 
            this.tb_ShortcutsKeyCollector.Location = new System.Drawing.Point(11, 138);
            this.tb_ShortcutsKeyCollector.Name = "tb_ShortcutsKeyCollector";
            this.tb_ShortcutsKeyCollector.Size = new System.Drawing.Size(293, 31);
            this.tb_ShortcutsKeyCollector.TabIndex = 10;
            this.tb_ShortcutsKeyCollector.Text = "[click here]";
            this.tb_ShortcutsKeyCollector.Click += new System.EventHandler(this.tb_ShortcutsKeyCollector_Click);
            this.tb_ShortcutsKeyCollector.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tb_ShortcutsKeyCollector_KeyDown);
            // 
            // btn_goToIndexNumber
            // 
            this.btn_goToIndexNumber.Location = new System.Drawing.Point(6, 30);
            this.btn_goToIndexNumber.Name = "btn_goToIndexNumber";
            this.btn_goToIndexNumber.Size = new System.Drawing.Size(190, 50);
            this.btn_goToIndexNumber.TabIndex = 9;
            this.btn_goToIndexNumber.Text = "Go to Index";
            this.btn_goToIndexNumber.UseVisualStyleBackColor = true;
            this.btn_goToIndexNumber.Click += new System.EventHandler(this.btn_goToIndexNumber_Click);
            // 
            // lbl_numImages
            // 
            this.lbl_numImages.AutoSize = true;
            this.lbl_numImages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_numImages.Location = new System.Drawing.Point(355, 40);
            this.lbl_numImages.Name = "lbl_numImages";
            this.lbl_numImages.Size = new System.Drawing.Size(39, 29);
            this.lbl_numImages.TabIndex = 9;
            this.lbl_numImages.Text = "/ 0";
            // 
            // tb_currentImageIndex
            // 
            this.tb_currentImageIndex.Location = new System.Drawing.Point(202, 40);
            this.tb_currentImageIndex.Name = "tb_currentImageIndex";
            this.tb_currentImageIndex.Size = new System.Drawing.Size(146, 31);
            this.tb_currentImageIndex.TabIndex = 9;
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(1064, 30);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(128, 128);
            this.btn_delete.TabIndex = 8;
            this.btn_delete.Text = "Delete";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_Load
            // 
            this.btn_Load.Location = new System.Drawing.Point(6, 208);
            this.btn_Load.Name = "btn_Load";
            this.btn_Load.Size = new System.Drawing.Size(190, 50);
            this.btn_Load.TabIndex = 4;
            this.btn_Load.Text = "Load Images";
            this.btn_Load.UseVisualStyleBackColor = true;
            this.btn_Load.Click += new System.EventHandler(this.btn_Load_Click);
            // 
            // btn_Undefined
            // 
            this.btn_Undefined.Location = new System.Drawing.Point(930, 30);
            this.btn_Undefined.Name = "btn_Undefined";
            this.btn_Undefined.Size = new System.Drawing.Size(128, 128);
            this.btn_Undefined.TabIndex = 7;
            this.btn_Undefined.Text = "Type Undefined";
            this.btn_Undefined.UseVisualStyleBackColor = true;
            this.btn_Undefined.Click += new System.EventHandler(this.btn_Undefined_Click);
            // 
            // btn_typeA
            // 
            this.btn_typeA.Location = new System.Drawing.Point(528, 30);
            this.btn_typeA.Name = "btn_typeA";
            this.btn_typeA.Size = new System.Drawing.Size(128, 128);
            this.btn_typeA.TabIndex = 1;
            this.btn_typeA.Text = "Type A";
            this.btn_typeA.UseVisualStyleBackColor = true;
            this.btn_typeA.Click += new System.EventHandler(this.btn_typeA_Click);
            // 
            // btn_PreviousImage
            // 
            this.btn_PreviousImage.Location = new System.Drawing.Point(1234, 30);
            this.btn_PreviousImage.Name = "btn_PreviousImage";
            this.btn_PreviousImage.Size = new System.Drawing.Size(128, 128);
            this.btn_PreviousImage.TabIndex = 6;
            this.btn_PreviousImage.Text = "Previous";
            this.btn_PreviousImage.UseVisualStyleBackColor = true;
            this.btn_PreviousImage.Click += new System.EventHandler(this.btn_PreviousImage_Click);
            // 
            // btn_typeB
            // 
            this.btn_typeB.Location = new System.Drawing.Point(662, 30);
            this.btn_typeB.Name = "btn_typeB";
            this.btn_typeB.Size = new System.Drawing.Size(128, 128);
            this.btn_typeB.TabIndex = 2;
            this.btn_typeB.Text = "Type B";
            this.btn_typeB.UseVisualStyleBackColor = true;
            this.btn_typeB.Click += new System.EventHandler(this.btn_typeB_Click);
            // 
            // btn_NextImage
            // 
            this.btn_NextImage.Location = new System.Drawing.Point(1368, 30);
            this.btn_NextImage.Name = "btn_NextImage";
            this.btn_NextImage.Size = new System.Drawing.Size(128, 128);
            this.btn_NextImage.TabIndex = 5;
            this.btn_NextImage.Text = "Next";
            this.btn_NextImage.UseVisualStyleBackColor = true;
            this.btn_NextImage.Click += new System.EventHandler(this.btn_NextImage_Click);
            // 
            // btn_typeC
            // 
            this.btn_typeC.Location = new System.Drawing.Point(796, 30);
            this.btn_typeC.Name = "btn_typeC";
            this.btn_typeC.Size = new System.Drawing.Size(128, 128);
            this.btn_typeC.TabIndex = 3;
            this.btn_typeC.Text = "Type C";
            this.btn_typeC.UseVisualStyleBackColor = true;
            this.btn_typeC.Click += new System.EventHandler(this.btn_typeC_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(8, 39);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1514, 1006);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_delete_name);
            this.groupBox1.Controls.Add(this.tb_delete);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tb_undefined_name);
            this.groupBox1.Controls.Add(this.tb_typeC_name);
            this.groupBox1.Controls.Add(this.tb_classificationFolder_name);
            this.groupBox1.Controls.Add(this.tb_typeB_name);
            this.groupBox1.Controls.Add(this.tb_typeA_name);
            this.groupBox1.Controls.Add(this.tb_undefined);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tb_typeC);
            this.groupBox1.Controls.Add(this.tb_classificationFolder);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb_typeB);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb_typeA);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1502, 483);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Classification Data (Restart after making changes!)";
            // 
            // tb_delete_name
            // 
            this.tb_delete_name.Location = new System.Drawing.Point(176, 358);
            this.tb_delete_name.Name = "tb_delete_name";
            this.tb_delete_name.Size = new System.Drawing.Size(326, 31);
            this.tb_delete_name.TabIndex = 19;
            // 
            // tb_delete
            // 
            this.tb_delete.Location = new System.Drawing.Point(508, 358);
            this.tb_delete.Name = "tb_delete";
            this.tb_delete.Size = new System.Drawing.Size(988, 31);
            this.tb_delete.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 358);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 25);
            this.label8.TabIndex = 17;
            this.label8.Text = "Type Delete";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(620, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 25);
            this.label7.TabIndex = 16;
            this.label7.Text = "Type Folder:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(171, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 25);
            this.label6.TabIndex = 15;
            this.label6.Text = "Type Name:";
            // 
            // tb_undefined_name
            // 
            this.tb_undefined_name.Location = new System.Drawing.Point(176, 308);
            this.tb_undefined_name.Name = "tb_undefined_name";
            this.tb_undefined_name.Size = new System.Drawing.Size(326, 31);
            this.tb_undefined_name.TabIndex = 14;
            // 
            // tb_typeC_name
            // 
            this.tb_typeC_name.Location = new System.Drawing.Point(176, 258);
            this.tb_typeC_name.Name = "tb_typeC_name";
            this.tb_typeC_name.Size = new System.Drawing.Size(326, 31);
            this.tb_typeC_name.TabIndex = 13;
            // 
            // tb_classificationFolder_name
            // 
            this.tb_classificationFolder_name.Location = new System.Drawing.Point(176, 111);
            this.tb_classificationFolder_name.Name = "tb_classificationFolder_name";
            this.tb_classificationFolder_name.Size = new System.Drawing.Size(326, 31);
            this.tb_classificationFolder_name.TabIndex = 10;
            // 
            // tb_typeB_name
            // 
            this.tb_typeB_name.Location = new System.Drawing.Point(176, 208);
            this.tb_typeB_name.Name = "tb_typeB_name";
            this.tb_typeB_name.Size = new System.Drawing.Size(326, 31);
            this.tb_typeB_name.TabIndex = 12;
            // 
            // tb_typeA_name
            // 
            this.tb_typeA_name.Location = new System.Drawing.Point(176, 158);
            this.tb_typeA_name.Name = "tb_typeA_name";
            this.tb_typeA_name.Size = new System.Drawing.Size(326, 31);
            this.tb_typeA_name.TabIndex = 11;
            // 
            // tb_undefined
            // 
            this.tb_undefined.Location = new System.Drawing.Point(508, 308);
            this.tb_undefined.Name = "tb_undefined";
            this.tb_undefined.Size = new System.Drawing.Size(988, 31);
            this.tb_undefined.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 308);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(164, 25);
            this.label5.TabIndex = 8;
            this.label5.Text = "Type Undefined";
            // 
            // tb_typeC
            // 
            this.tb_typeC.Location = new System.Drawing.Point(508, 258);
            this.tb_typeC.Name = "tb_typeC";
            this.tb_typeC.Size = new System.Drawing.Size(988, 31);
            this.tb_typeC.TabIndex = 7;
            // 
            // tb_classificationFolder
            // 
            this.tb_classificationFolder.Location = new System.Drawing.Point(508, 111);
            this.tb_classificationFolder.Name = "tb_classificationFolder";
            this.tb_classificationFolder.Size = new System.Drawing.Size(988, 31);
            this.tb_classificationFolder.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Root Folder";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 261);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Type C";
            // 
            // tb_typeB
            // 
            this.tb_typeB.Location = new System.Drawing.Point(508, 208);
            this.tb_typeB.Name = "tb_typeB";
            this.tb_typeB.Size = new System.Drawing.Size(988, 31);
            this.tb_typeB.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Type B";
            // 
            // tb_typeA
            // 
            this.tb_typeA.Location = new System.Drawing.Point(508, 158);
            this.tb_typeA.Name = "tb_typeA";
            this.tb_typeA.Size = new System.Drawing.Size(988, 31);
            this.tb_typeA.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Type A";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lbl_About);
            this.tabPage3.Location = new System.Drawing.Point(8, 39);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1514, 1006);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "About";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lbl_About
            // 
            this.lbl_About.AutoSize = true;
            this.lbl_About.Location = new System.Drawing.Point(619, 408);
            this.lbl_About.Name = "lbl_About";
            this.lbl_About.Size = new System.Drawing.Size(212, 25);
            this.lbl_About.TabIndex = 0;
            this.lbl_About.Text = "Author: Matija Bensa";
            // 
            // lbl_key_typeA
            // 
            this.lbl_key_typeA.AutoSize = true;
            this.lbl_key_typeA.Location = new System.Drawing.Point(523, 184);
            this.lbl_key_typeA.Name = "lbl_key_typeA";
            this.lbl_key_typeA.Size = new System.Drawing.Size(26, 25);
            this.lbl_key_typeA.TabIndex = 13;
            this.lbl_key_typeA.Text = "A";
            // 
            // lbl_key_typeB
            // 
            this.lbl_key_typeB.AutoSize = true;
            this.lbl_key_typeB.Location = new System.Drawing.Point(657, 184);
            this.lbl_key_typeB.Name = "lbl_key_typeB";
            this.lbl_key_typeB.Size = new System.Drawing.Size(26, 25);
            this.lbl_key_typeB.TabIndex = 14;
            this.lbl_key_typeB.Text = "S";
            // 
            // lbl_key_typeC
            // 
            this.lbl_key_typeC.AutoSize = true;
            this.lbl_key_typeC.Location = new System.Drawing.Point(791, 184);
            this.lbl_key_typeC.Name = "lbl_key_typeC";
            this.lbl_key_typeC.Size = new System.Drawing.Size(27, 25);
            this.lbl_key_typeC.TabIndex = 15;
            this.lbl_key_typeC.Text = "D";
            // 
            // lbl_key_typeUndefined
            // 
            this.lbl_key_typeUndefined.AutoSize = true;
            this.lbl_key_typeUndefined.Location = new System.Drawing.Point(925, 184);
            this.lbl_key_typeUndefined.Name = "lbl_key_typeUndefined";
            this.lbl_key_typeUndefined.Size = new System.Drawing.Size(25, 25);
            this.lbl_key_typeUndefined.TabIndex = 16;
            this.lbl_key_typeUndefined.Text = "F";
            // 
            // lbl_key_typeDelete
            // 
            this.lbl_key_typeDelete.AutoSize = true;
            this.lbl_key_typeDelete.Location = new System.Drawing.Point(1059, 184);
            this.lbl_key_typeDelete.Name = "lbl_key_typeDelete";
            this.lbl_key_typeDelete.Size = new System.Drawing.Size(23, 25);
            this.lbl_key_typeDelete.TabIndex = 17;
            this.lbl_key_typeDelete.Text = "J";
            // 
            // lbl_key_previous
            // 
            this.lbl_key_previous.AutoSize = true;
            this.lbl_key_previous.Location = new System.Drawing.Point(1229, 184);
            this.lbl_key_previous.Name = "lbl_key_previous";
            this.lbl_key_previous.Size = new System.Drawing.Size(26, 25);
            this.lbl_key_previous.TabIndex = 18;
            this.lbl_key_previous.Text = "K";
            // 
            // lbl_key_next
            // 
            this.lbl_key_next.AutoSize = true;
            this.lbl_key_next.Location = new System.Drawing.Point(1363, 184);
            this.lbl_key_next.Name = "lbl_key_next";
            this.lbl_key_next.Size = new System.Drawing.Size(24, 25);
            this.lbl_key_next.TabIndex = 19;
            this.lbl_key_next.Text = "L";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1059, 221);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 25);
            this.label10.TabIndex = 20;
            this.label10.Text = "N=J+K";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1542, 1077);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.btn_Previous_FormClosing);
            this.Load += new System.EventHandler(this.btn_Previous_Load);
            this.Resize += new System.EventHandler(this.btn_Previous_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.gb_commands.ResumeLayout(false);
            this.gb_commands.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox tb_classificationFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_typeC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_typeB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_typeA;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_typeC;
        private System.Windows.Forms.Button btn_typeB;
        private System.Windows.Forms.Button btn_typeA;
        private System.Windows.Forms.Button btn_Load;
        private System.Windows.Forms.Button btn_PreviousImage;
        private System.Windows.Forms.Button btn_NextImage;
        private System.Windows.Forms.Button btn_Undefined;
        private System.Windows.Forms.TextBox tb_undefined;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox gb_commands;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_undefined_name;
        private System.Windows.Forms.TextBox tb_typeC_name;
        private System.Windows.Forms.TextBox tb_classificationFolder_name;
        private System.Windows.Forms.TextBox tb_typeB_name;
        private System.Windows.Forms.TextBox tb_typeA_name;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.TextBox tb_delete_name;
        private System.Windows.Forms.TextBox tb_delete;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_numImages;
        private System.Windows.Forms.TextBox tb_currentImageIndex;
        private System.Windows.Forms.Button btn_goToIndexNumber;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label lbl_About;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_ShortcutsKeyCollector;
        private System.Windows.Forms.Label lbl_imageName;
        private System.Windows.Forms.Label lbl_key_next;
        private System.Windows.Forms.Label lbl_key_previous;
        private System.Windows.Forms.Label lbl_key_typeDelete;
        private System.Windows.Forms.Label lbl_key_typeUndefined;
        private System.Windows.Forms.Label lbl_key_typeC;
        private System.Windows.Forms.Label lbl_key_typeB;
        private System.Windows.Forms.Label lbl_key_typeA;
        private System.Windows.Forms.Label label10;
    }
}

