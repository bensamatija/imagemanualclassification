﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Configuration;
using System.Timers;
using System.Diagnostics;

using VB = Microsoft.VisualBasic.FileIO;

using System.Threading;



namespace ImageManualClassification
{
    /// <summary>
    /// Images list and Images Operation holder
    /// </summary>
    public class ImagesForClassification
    {
        public List<TheImage> listOfImages = new List<TheImage>();
        public int currentImageIndex = 0;
        public bool typeChanged = false;

        public void LoadImagesToTheList(string dir)
        {
            listOfImages.Clear();
            string[] listOfFiles = Directory.GetFiles(dir, "*.*", SearchOption.AllDirectories);
            foreach (string f in listOfFiles)
            {
                //TheImage theImage = new TheImage(TheImage.ClassType.UNDEFINED, f);
                TheImage theImage = new TheImage(f);
                //listOfImages.Add(new TheImage(f));
                //theImage.GetClassByDestinationFolder();
                //var a = theImage.GetClassByDestinationFolder();

                listOfImages.Add(new TheImage(theImage.GetClassByDestinationFolder(), f));
            }
        }

        public void SortListOfImages()
        {
            listOfImages.Sort((x, y) => x.imageFile.CompareTo(y.imageFile));
        }

        public void NextImage()
        {
            currentImageIndex++;

            if (currentImageIndex > listOfImages.Count - 1)
            {
                currentImageIndex = 0;
            }
        }

        public void PreviousImage()
        {
            currentImageIndex--;

            if (currentImageIndex < 0)
            {
                currentImageIndex = listOfImages.Count - 1;
            }
        }

        public void DisplayImage(PictureBox pb)
        {
            //pb.SizeMode = PictureBoxSizeMode.Zoom;
            //pb.Show();
            pb.ImageLocation = GetCurrentimage().imageFile;
        }

        public void SetClassification(TheImage.ClassType ct)
        {
            listOfImages[currentImageIndex].classType = ct;
            typeChanged = true;
        }

        public void SaveImageClassType()
        {
            if (typeChanged == true)
            {
                string filePath = GetCurrentimage().imageFile;
                string fileName = Path.GetFileName(GetCurrentimage().imageFile);
                string destinationDir = GetCurrentimage().GetDestinationFolderByClass();
                string destinationFullName = destinationDir + "\\" + fileName;

                if (filePath != destinationFullName)
                {
                    int n = 1;
                    string filenameNoExt = Path.GetFileNameWithoutExtension(fileName);
                    string extension = Path.GetExtension(fileName);
                    while (File.Exists(destinationFullName))
                    {
                        destinationFullName = destinationDir + "\\" + filenameNoExt + "_" + n.ToString() + extension;
                        n++;
                    }
                    File.Move(filePath, destinationFullName);
                }

                // Save New file destination to object:
                GetCurrentimage().imageFile = destinationFullName;

                typeChanged = false;
            }
        }

        public string GetImageFolder()
        {
            return listOfImages[currentImageIndex].imageFile;
        }

        public TheImage GetCurrentimage()
        {
            return listOfImages[currentImageIndex];
        }

        public TheImage.ClassType GetClass()
        {
            return listOfImages[currentImageIndex].GetClass();
        }

        public void GoToImageIndex(int index)
        {
            currentImageIndex = index;
        }
    }

    /// <summary>
    /// Image Properties
    /// </summary>
    public class TheImage
    {
        public ClassType classType;
        public string imageFile;

        public enum ClassType
        {
            TypeA, TypeB, TypeC, UNDEFINED, DELETE
        }

        public ClassType GetClass()
        {
            return classType;
        }

        public string GetImageFilePath()
        {
            return imageFile;
        }

        public string GetImageFileFolder()
        {
            var folder = Path.GetDirectoryName(GetImageFilePath());
            return folder;
        }

        public ClassType GetClassByDestinationFolder()
        {
            ClassType ct = ClassType.UNDEFINED;
            string dir = GetDestinationFolderByClass();
            string dir2 = GetImageFileFolder();
            string folder = Path.GetDirectoryName(imageFile);

            if (folder == Properties.Settings.Default.dirTypeA) { ct = ClassType.TypeA; }
            else if (folder == Properties.Settings.Default.dirTypeB) { ct = ClassType.TypeB; }
            else if (folder == Properties.Settings.Default.dirTypeC) { ct = ClassType.TypeC; }
            else if (folder == Properties.Settings.Default.dirTypeUndefined) { ct = ClassType.UNDEFINED; }
            else if (folder == Properties.Settings.Default.dirTypeDelete) { ct = ClassType.DELETE; }

            return ct;
        }

        public Color GetColorByClass()
        {
            Color c = new Color();
            switch (classType)
            {
                case ClassType.TypeA: c = Color.Blue; break;
                case ClassType.TypeB: c = Color.Green; break;
                case ClassType.TypeC: c = Color.Pink; break;
                case ClassType.UNDEFINED: c = Color.Orange; break;
                case ClassType.DELETE: c = Color.Red; break;
            }
            return c;
        }

        public string GetDestinationFolderByClass()
        {
            string dir = "";
            switch (classType)
            {
                case ClassType.TypeA: dir = Properties.Settings.Default.dirTypeA; break;
                case ClassType.TypeB: dir = Properties.Settings.Default.dirTypeB; break;
                case ClassType.TypeC: dir = Properties.Settings.Default.dirTypeC; break;
                case ClassType.UNDEFINED: dir = Properties.Settings.Default.dirTypeUndefined; break;
                case ClassType.DELETE: dir = Properties.Settings.Default.dirTypeDelete; break;
            }
            return dir;
        }

        public TheImage()
        {
            // Empty constructor
        }

        public TheImage(string imageFile)
        {
            this.imageFile = imageFile;
        }

        public TheImage(ClassType classType, string imageFile)
        {
            this.classType = classType;
            this.imageFile = imageFile;
        }
    }

}
